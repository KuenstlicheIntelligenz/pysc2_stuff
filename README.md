# pysc2_stuff

A collection of infos for pysc2 and scripts

## bin/sort_replays

A script to sort replays into folders
example: '/p1_wins/tvp/123.sc2replay'

## Queues
```python
observation.build_queue
```
Gives the build queue of the selected building and
will be something like

```python
[[45  0  0  0  0  0 63]
 [45  0  0  0  0  0  0]]
```

and that probably means

```python
[unit_id ? ? ? ? ? percentage_finished]
```

45 is the scv of the terrans. They can only build one at a time, so the percentage of the second entry
won't change.

## unit_counts
```python
[[unit_id count], [unit_id, count], ...]
```

## feature_units
Coordinates of the center of a unit