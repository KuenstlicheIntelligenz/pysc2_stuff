#!/usr/bin/python
# Copyright 2017 Google Inc. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS-IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""
This script sorts replays in folder in:

p1_wins / p2_wins

and in the subfolders

['tvt', 'tvp', 'tvz', 'pvt', 'pvp', 'pvz', 'zvt', 'zvp', 'zvz']

I've just modified the replay_info.py from the pysc2 package
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os

from absl import app
from future.builtins import str  # pylint: disable=redefined-builtin

from pysc2 import run_configs
from pysc2.lib import remote_controller

from pysc2.lib import gfile
from s2clientprotocol import common_pb2 as sc_common
from s2clientprotocol import sc2api_pb2 as sc_pb


def _replay_index(replay_dir):
    """Output information for a directory of replays."""
    run_config = run_configs.get()
    replay_dir = run_config.abs_replay_path(replay_dir)


    ## Check if needed dirs exist, else create them
    acro_map = {'Protoss': 'p', 'Zerg': 'z', 'Terran': 't'}
    race_acronyms = ['t', 'p', 'z']
    matchups = []
    for acro in race_acronyms:
        for acro2 in race_acronyms:
            matchups.append(acro + 'v' + acro2)
    folder_names = ['p1_wins', 'p2_wins']
    for folder_name in folder_names:
        full_path = replay_dir + '/' + folder_name
        if not os.path.exists(full_path):
            os.makedirs(full_path)
        for matchup in matchups:
            if not os.path.exists(full_path + '/' + matchup):
                os.makedirs(full_path + '/' + matchup)

    print("Checking: ", replay_dir)

    with run_config.start(want_rgb=False) as controller:
        try:
            for file_path in run_config.replay_paths(replay_dir):
                file_name = os.path.basename(file_path)
                try:
                    info = controller.replay_info(run_config.replay_data(file_path))
                except remote_controller.RequestError as e:
                    os.remove(file_path)
                    continue
                if info.HasField("error"):
                    os.remove(file_path)
                else:
                    if len(info.player_info) < 2:
                        os.remove(file_path)
                        continue

                    p1_result = sc_pb.Result.Name(info.player_info[0].player_result.result)
                    if p1_result == 'Victory':
                        base_dir = replay_dir + '/p1_wins'
                    else:
                        base_dir = replay_dir + '/p2_wins'

                    race_p1 = sc_common.Race.Name(info.player_info[0].player_info.race_actual)
                    race_p2 = sc_common.Race.Name(info.player_info[1].player_info.race_actual)

                    acronym_p1 = acro_map[race_p1]
                    acronym_p2 = acro_map[race_p2]

                    matchup = acronym_p1 + 'v' + acronym_p2

                    full_dir = base_dir + '/' + matchup

                    os.rename(src=file_path, dst=full_dir + '/' + file_name)
        except KeyboardInterrupt:
            pass


def _replay_info(replay_path):
    """Sort replays"""
    if not replay_path.lower().endswith("sc2replay"):
        print("Must be a replay.")
        return

    run_config = run_configs.get()
    with run_config.start(want_rgb=False) as controller:
        info = controller.replay_info(run_config.replay_data(replay_path))
    print("-" * 60)
    print(info)


def main(argv):
    if not argv:
        raise ValueError("No replay directory or path specified.")
    if len(argv) > 2:
        raise ValueError("Too many arguments provided.")
    path = argv[1]

    try:
        if gfile.IsDirectory(path):
            return _replay_index(path)
        else:
            return _replay_info(path)
    except KeyboardInterrupt:
        pass



if __name__ == "__main__":
    app.run(main)
